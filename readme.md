# Logging program and post processing scripts for Passive Atcive VLP

The logging program is written in [go](https://golang.org/). More info on this is in the readme in the go directory. 

The post processing scripts are written in python.
