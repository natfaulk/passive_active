import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import loader
import process

# calculated by running and looking at the graphs
LIGHT1 = [-0.988, 2.501, 1.810]
LIGHT2 = [-1.012 ,2.526, 3.413]
LIGHT3 = [-2.167 ,2.511, 3.384]
LIGHT4 = [-2.193 ,2.522, 1.808]

PD1=[-1.402, 0.986, 0.843]
PD2=[ 0.942, 1.016, 1.906]
PD3=[ 0.92 , 1.03 , 3.215]
PD4=[-3.827, 1.011, 3.144]
PD5=[-3.813, 0.998, 1.955]

def getXY(_df, _range):
  start=_range[0]
  end=_range[1]

  fig, axs = plt.subplots(3)
  axs[0].plot(_df['x'].loc[start:end])
  axs[1].plot(_df['y'].loc[start:end])
  axs[2].plot(_df['z'].loc[start:end])
  plt.show()

if __name__ == '__main__':

  vive_df=loader.vive('lightLayout.txt')
  vive_data=process.vive(vive_df)

  START_TIME=vive_df.iloc[0].time
  print('START TIME:', START_TIME)

  vive_data=np.array(vive_data)
  df=pd.DataFrame(vive_data)
  df.columns=['x', 'y', 'z']
  df['time']=vive_df['time']-START_TIME

  # uncomment for ceiling lights
  # df=df[df.y > 2.0]

  #for wall pds
  # df=df[df.y < 1.2]
  # df=df[df.y > 0.8]
  
  print(df.head())

  fig, axs = plt.subplots(2, 2)
  axs[0, 0].scatter(df['x'], df['y'])
  axs[0, 0].set(xlabel='x', ylabel='y')
  axs[0, 1].scatter(df['z'], df['y'])
  axs[0, 1].set(xlabel='z', ylabel='y')
  axs[1, 0].scatter(df['x'], df['z'])
  axs[1, 0].set(xlabel='x', ylabel='z')

  my3d = fig.add_subplot(2, 2, 4, projection='3d')
  my3d.scatter(df['x'], df['z'], df['y'])
  plt.show()

  fig, axs = plt.subplots(3)
  axs[0].plot(df['x'])
  axs[1].plot(df['y'])
  axs[2].plot(df['z'])
  plt.show()

  # uncomment for ceiling lights
  # range1=[10700,11200]
  # range2=[12200,12500]
  # range3=[13700,14050]
  # range4=[14900,15400]

  range1=[3900,4500]
  range2=[5000,5650]
  range3=[6000,6200]
  range4=[7100,7650]
  range5=[8000,8500]
  
  getXY(df, range1)
  getXY(df, range2)
  getXY(df, range3)
  getXY(df, range4)

  # only for wall pds
  getXY(df, range5)
