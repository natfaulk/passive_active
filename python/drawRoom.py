import matplotlib.pyplot as plt
import numpy as np

import roomConfig

if __name__ == '__main__':
  lpos = np.array(roomConfig.LIGHT_POSITIONS)
  lpos = np.delete(lpos,1,1).T.tolist()

  pdpos = np.array(roomConfig.PD_POSITIONS)
  pdpos = np.delete(pdpos,1,1).T.tolist()

  plt.scatter(*lpos)
  plt.scatter(*pdpos)
  plt.xlim([-4,1])
  plt.ylim([0,5])
  plt.show()

