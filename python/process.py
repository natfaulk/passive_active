import math
import sys

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd

# x axis is inverted.
# ie looking from lighthouse pespective - +x is left, +y up, +z forward
actuals = [
  [-2.0, 0.0, 1.0 ],
  [-1.0, 0.0, 1.0 ],
  [ 0.0, 0.0, 1.0 ],
  [ 0.0, 0.0, 2.0 ],
  [ 0.0, 0.0, 3.0 ]
]

ANCHOR_COUNT = len(actuals)

def vive(_data, _drawing=False):
  intervals = []
  start = 0
  prev = False
  for i, row in _data.iterrows():
    if row['y'] < -1.0:
      if not prev:
        start = i
        prev = True
    else:
      if prev:
        intervals.append([start, i])
        prev = False

  if prev:
    intervals.append([start, i])
  
  # get midpoints and use them as the cordinates. Might be better to average over a larger section
  mids = []
  count = 1
  # print midpoint
  for i in intervals:
    if count > ANCHOR_COUNT:
      print("Too many anchors - check this!")
      break
    temp=_data.iloc[(i[0]+i[1])//2,:]
    # print(count, temp['x'], temp['y'], temp['z'])
    mids.append([temp['x'], temp['y'], temp['z']])
    count+=1

  if len(mids) < ANCHOR_COUNT:
    sys.exit('Not enough calibration points')
  
  THETA = math.atan2(
    (mids[2][2]-mids[0][2]),
    (mids[2][0]-mids[0][0])
  )

  mids_rot = []
  for i in mids:
    mids_rot.append(rotate(i, THETA, 'y'))

  mids_trans = []
  dX = actuals[0][0]-mids_rot[0][0]
  dY = actuals[0][1]-mids_rot[0][1]
  dZ = actuals[0][2]-mids_rot[0][2]
  for i in mids_rot:
    mids_trans.append(translate(i, [dX,dY,dZ]))

  # drawing
  if _drawing:  
    actuals_df = pd.DataFrame(actuals)
    actuals_df.columns = ['x', 'y', 'z']
    mids_df = pd.DataFrame(mids)
    mids_df.columns = ['x', 'y', 'z']
    mids_trans_df = pd.DataFrame(mids_trans)
    mids_trans_df.columns = ['x', 'y', 'z']
    mids_rot_df = pd.DataFrame(mids_rot)
    mids_rot_df.columns = ['x', 'y', 'z']

    fig = plt.figure()
    ax = Axes3D(fig)
    ax.set_xlim3d(-4, 4)
    ax.set_ylim3d(-4, 4)
    ax.set_zlim3d(-4, 4)
    ax.set_xlabel('X (m)')
    ax.set_ylabel('Z (m)')
    ax.set_zlabel('Y (m)')

    ax.scatter(actuals_df['x'], actuals_df['z'], actuals_df['y'])
    ax.scatter(mids_df['x'], mids_df['z'], mids_df['y'])
    ax.scatter(mids_trans_df['x'], mids_trans_df['z'], mids_trans_df['y'])
    ax.scatter(mids_rot_df['x'], mids_rot_df['z'], mids_rot_df['y'])
    plt.show()
  
  out = []
  for i, row in _data.iterrows():
    temp = rotate([row['x'],row['y'],row['z']], THETA, 'y')
    out.append(translate(temp, [dX,dY,dZ]))

  df=pd.DataFrame(out)
  df.columns=['x', 'y', 'z']
  df['time']=_data['time']

  return df

# As the vive and PD timestamps do not match up exactly, interp between vive
# points so that they match up to the photodiode timestamps and add them to 
# the PD dataframe
def addViveToPdDf(df_pd, df_vive):
  pdTimestamps=df_pd['time'].tolist()
  vivetime=df_vive['time'].tolist()
  vivedata=np.transpose([
    df_vive['x'].values.tolist(),
    df_vive['y'].values.tolist(),
    df_vive['z'].values.tolist()
  ])

  out=[]
  j=0
  for t in pdTimestamps:
    while(vivetime[j+1]<t):
      j+=1

    #vive time
    vt1=vivetime[j]
    vt2=vivetime[j+1]
    
    #time diff
    td1=(t-vt1).total_seconds()
    td2=(vt2-t).total_seconds()

    lerp = td1/(td1+td2)

    # vive val
    vv1=vivedata[j]
    vv2=vivedata[j+1]

    # interped vive vals
    vv=[]

    # xyz vals
    for k in range(len(vv1)):
      vv.append(vv1[k]*(1-lerp)+vv2[k]*(lerp))
    
    out.append(vv)
  
  df_pd['pos']=out

def translate(point, trans):
  point2 = point + [1]
  trans_mat = np.array([[1, 0, 0, trans[0]],
                        [0, 1, 0, trans[1]],
                        [0, 0, 1, trans[2]],
                        [0, 0, 0, 1]])
  return trans_mat.dot(point2).tolist()[:3]

def rotate(point, angle, axis):
  rotMat = []
  if (axis == 'x'):
    rotMat = np.array([[1, 0, 0], [0, math.cos(angle), -math.sin(angle)], [0, math.sin(angle), math.cos(angle)]])
  if (axis == 'y'):
    rotMat = np.array([[math.cos(angle), 0, math.sin(angle)],[0, 1, 0], [-math.sin(angle), 0, math.cos(angle)]])
  if (axis == 'z'):
    rotMat = np.array([[math.cos(angle), -math.sin(angle), 0], [math.sin(angle), math.cos(angle), 0], [0, 0, 1]])

  return rotMat.dot(point).tolist()[:3]
