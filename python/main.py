import os

import pandas as pd

import links
import loader
import roomConfig


datasets={
  'ds1': {
    'pd': 'PD_2021-02-28_01-25-41.txt',
    'vive':'pas_act0.txt'
  },
  'ds2': {
    'pd': 'PD_2021-02-28_01-51-18.txt',
    'vive':'pas_act1.txt'
  },
  'ds3': {
    'pd': 'PD_2021-02-28_01-33-51.txt',
    'vive': None
  },
  'ds4': {
    'pd': 'PD_2021-02-28_02-02-27.txt',
    'vive': None
  }
}

def genCsv(_toload):
  COMBINED_FILENAME=f'{_toload}.json'
  # Load all the data. If it has not been loaded before and preprocessed,
  # preprocess it and save to file
  pd_df,vive_df=loader.combinedFromFile(COMBINED_FILENAME)
  if pd_df is None:
    print('Data not pre-processed. Combining vive and thermo data...')
    pd_df,vive_df=loader.combined(datasets[_toload]['pd'], datasets[_toload]['vive'])
    loader.combinedToFile(pd_df,vive_df, COMBINED_FILENAME)

  # loop through all the rows and get the peaks from the FFT
  out=[]
  for i, row in pd_df.iterrows():
    peaks = links.getPeaks(row['data'], roomConfig.LIGHT_FREQS)

    out2={}
    for f in peaks:
      linkNum=roomConfig.getLinkNum(row['id'], f)
      out2[f'link_{linkNum}']=peaks[f]

    out2['time']=row['time']

    if vive_df is not None:
      # swap y and z axis. The vive treats x and z as horizontal and y as the vertical
      # (ie height above the ground)
      # more standard to have other way I think
      out2['x']=row['pos'][0]
      out2['y']=row['pos'][2]
      out2['z']=row['pos'][1]

    out.append(out2)

  # save to dataframe 
  df_final = pd.DataFrame(out)

  # fill empty values with last previous known value
  df_final=df_final.fillna(method='ffill')
  # reorder the columns so in alphabetical order
  df_final = df_final.reindex(sorted(df_final.columns), axis=1)
  
  # save to csv
  outputFilepath = os.path.join(loader.OUTPUT_DATA_PATH, f'{_toload}.csv')
  df_final.to_csv(outputFilepath)
  print(f'Output file written to {outputFilepath}')

if __name__ == '__main__':
  for ds in datasets:
    genCsv(ds)
