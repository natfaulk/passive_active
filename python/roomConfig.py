class Light:
  def __init__(self, pos, freq):
    self.pos = pos
    self.freq = freq

class PD_Board:
  def __init__(self, id, pos):
    self.id = id
    self.pos = pos

LIGHT1_POS = [-0.988, 2.501, 1.810]
LIGHT2_POS = [-1.012 ,2.526, 3.413]
LIGHT3_POS = [-2.167 ,2.511, 3.384]
LIGHT4_POS = [-2.193 ,2.522, 1.808]

LIGHT1_FREQ = 15 * 1000
LIGHT2_FREQ = 23 * 1000
LIGHT3_FREQ = 21 * 1000
LIGHT4_FREQ = 18 * 1000

LIGHT_FREQS = [
  LIGHT1_FREQ,
  LIGHT2_FREQ,
  LIGHT3_FREQ,
  LIGHT4_FREQ
]

LIGHT_POSITIONS = [
  LIGHT1_POS,
  LIGHT2_POS,
  LIGHT3_POS,
  LIGHT4_POS
]

LIGHTS = [
  Light(LIGHT1_POS, LIGHT1_FREQ),
  Light(LIGHT2_POS, LIGHT2_FREQ),
  Light(LIGHT3_POS, LIGHT3_FREQ),
  Light(LIGHT4_POS, LIGHT4_FREQ)
]

PD1_POS=[-1.402, 0.986, 0.843]
PD2_POS=[ 0.942, 1.016, 1.906]
PD3_POS=[ 0.92 , 1.03 , 3.215]
PD4_POS=[-3.827, 1.011, 3.144]
PD5_POS=[-3.813, 0.998, 1.955]

PD1_ID = 'VLP_REC3'
PD2_ID = 'VLP_REC7'
PD3_ID = 'VLP_REC1'
PD4_ID = 'VLP_REC2'
PD5_ID = 'VLP_REC8'

PD_POSITIONS = [
  PD1_POS,
  PD2_POS,
  PD3_POS,
  PD4_POS,
  PD5_POS
]

PD_IDS = [
  PD1_ID,
  PD2_ID,
  PD3_ID,
  PD4_ID,
  PD5_ID
]

PDS = [
  PD_Board(PD1_ID, PD1_POS),
  PD_Board(PD2_ID, PD2_POS),
  PD_Board(PD3_ID, PD3_POS),
  PD_Board(PD4_ID, PD4_POS),
  PD_Board(PD5_ID, PD5_POS)
]

def getLightFromFreq(freq):
  for i in LIGHTS:
    if i.freq == freq:
      return i
  
  print(f'Could not find light with frequency: {freq} Hz')
  return None

def getLightNumFromFreq(freq):
  for i, v in enumerate(LIGHTS):
    if v.freq == freq:
      return i 

  return None

def getPdNumFromID(id):
  for i, v in enumerate(PDS):
    if v.id == id:
      return i 

  return None

def getLinkNum(boardID, lightFreq):
  pdNum = getPdNumFromID(boardID)
  lightNum = getLightNumFromFreq(lightFreq)

  return lightNum*len(PDS) + pdNum
