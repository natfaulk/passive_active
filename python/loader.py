import gzip
import json
import os

import pandas as pd

import process

BASE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)))
DATA_PATH = os.path.join(BASE_PATH, '..', 'data')
VIVE_DATA_PATH = os.path.join(DATA_PATH, 'vive')
PD_DATA_PATH = os.path.join(DATA_PATH, 'pd')
COMBINED_DATA_PATH = os.path.join(DATA_PATH, 'combined')
OUTPUT_DATA_PATH = os.path.join(DATA_PATH, 'outputs')

PD_DATA_LINE_HEADER = 'ADC Data:'
PD_DATA_EXPECTED_LEN = 512

# make sure dirs exist
os.makedirs(COMBINED_DATA_PATH, exist_ok=True)
os.makedirs(OUTPUT_DATA_PATH, exist_ok=True)

def combined(pdFilename, viveFilename):
  pd_df = photodiodes(pdFilename)

  if viveFilename is None:
    return pd_df, None

  vive_df = vive(viveFilename)  

  print(f'Processing data...')
  vive_df=process.vive(vive_df)
  process.addViveToPdDf(pd_df, vive_df)

  return pd_df, vive_df

# Load a Photodiode data file
def photodiodes(filename):
  filepath = os.path.join(PD_DATA_PATH, filename)
  print(f'Loading photodiode data from {filepath}')

  
  with open(filepath) as f:
    lines = f.readlines()

  data = []
  for line in lines:
    l_decoded=decodePdLine(line)
    if l_decoded is not None:
      data.append(l_decoded)
  
  df = pd.DataFrame(data)
  # need to convert to int64 first else python tries to convert to a 32 bit int
  # when converting to datetime, and it crashes.
  df['time'] = df['time'].astype('int64')
  df['time'] = pd.to_datetime(df['time'], unit='ns')
  return df

# load a vive data file
def vive(filename):
  filepath = os.path.join(VIVE_DATA_PATH, filename)
  print(f'Loading vive data from {filepath}')

  with open(filepath) as f:
    lines = f.readlines() 

  data = []
  for l in lines:
    data.append(decodeViveLine(l))
  
  df = pd.DataFrame(data)
  df['time'] = pd.to_datetime(df['time'], unit='s')

  return df

# decodes a single photodiode line assuming each line is in the following format
# unixtime|id|ip|data
# where the data is in the format:
# ADC Data:[value1,value2, etc...]
def decodePdLine(line):
  vals=line.split('|')

  if len(vals) < 4:
    print('Invalid PD line: not enough values')
    return None

  # sometimes a packet gets written over another meaning there are two ADC Data: headers 
  # find the rightmost occurence of the data header and treat that as the start of the line
  dataString=vals[3]
  linestart=dataString.rfind(PD_DATA_LINE_HEADER)
  if linestart == -1:
    print('Invalid PD line: No data header found')
    return None
  dataString=dataString[linestart:]

  # remove data header before json decoding
  dataString=dataString[len(PD_DATA_LINE_HEADER):]

  try:
    parsedData=json.loads(dataString)
  except json.decoder.JSONDecodeError:
    print('Invalid PD line: Malformed JSON')
    return None

  # some lines have valid JSON, but its missing bits
  if len(parsedData) != PD_DATA_EXPECTED_LEN:
    print(f'Warning: PD line data is not of expected length. Expected {PD_DATA_EXPECTED_LEN}, was {len(parsedData)}')
    return None

  out={
    'time': vals[0],
    'id': vals[1],
    'data': parsedData
  }

  return out

# decodes vive line
# The vive data is much more reliable than the PD data
# Therefore minimal data format checking
def decodeViveLine(line):
  parsedLine = json.loads(line)

  out = {}
  out['time'] = parsedLine['time']
  out['x'] = parsedLine['pos'][0]
  out['y'] = parsedLine['pos'][1]
  out['z'] = parsedLine['pos'][2]
  out['roll'] = parsedLine['pos'][3]
  out['pitch'] = parsedLine['pos'][4]
  out['yaw'] = parsedLine['pos'][5]

  return out

# writes combined vive and pd data to file. Compresses with Gzip to save space
# github doesn't like files over 100mb and uncompressed can be larger than that 
# dependant on data length
def combinedToFile(pd_df, vive_df, _filename):
  FILENAME = os.path.join(COMBINED_DATA_PATH, _filename+'.gz')
  print(f'Saving combined vive thermo data to {FILENAME}')

  pd_out = dfToSerializable(pd_df)
  vive_out = dfToSerializable(vive_df)

  out = {
    'pd': pd_out,
    'vive': vive_out,
  }

  with gzip.open(FILENAME, 'wt') as outfile:
    json.dump(out, outfile)

# make dataframe serializable so can be written to output
# made into seperate function to make handling None (ie no df) values easier
def dfToSerializable(df):
  if df is None:
    return None

  # so we don't edit the current dataframe
  df_out = df.copy()

  # need to change datetime to string as otherwise complains it is not serializable
  # TypeError: Object of type Timestamp is not JSON serializable
  df_out['time']=df_out['time'].astype(str)

  return df_out.to_dict('records')

# retrieve data from combined file. Returns the pd and vive dataframes 
# or None if loading failed - either the file doesn't exist or another reason
def combinedFromFile(_filename):
  FILENAME = os.path.join(COMBINED_DATA_PATH, _filename+'.gz')
  print(f'Loading combined vive thermo data from {FILENAME}')

  try:
    with gzip.open(FILENAME, 'rt') as f:
      # want it to fail on a json load error - leave it to crash instead
      # of a try except
      data = json.load(f)
      pd_df=pd.DataFrame(data['pd'])
      
      # convert time columns back to datetimes
      pd_df['time'] = pd.to_datetime(pd_df['time'])
      
      if data['vive'] is None:
        return pd_df, None

      # same for the Vive as with the PD data if it exists
      vive_df=pd.DataFrame(data['vive'])
      vive_df['time'] = pd.to_datetime(vive_df['time'])

      return pd_df, vive_df
  except IOError:
    print(f'File {FILENAME} does not exist.')
    return None, None



