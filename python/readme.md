# Python Post Processing Scripts

`main.py` takes the data files, extracts the link values between the lights and links them to the HTC vive positions.

`roomConfig.py` contains lots of variables related to the room setup.

`roomLayoutCalc.py` is used to calculate the positions of the lights and photodiodes from the vive data. Unfortunately it is not very well written and is a fairly manual process. Hovering over the values on the graphs...

`drawRoom.py` draws the room layout.

The project dependencies are in requirements.txt

I reccommend running `python -m pip install -r requirements.txt` to install the dependencies.

Requires python 3 because python 2 is very outdated and people really shouldn't be starting new projects with it.....

