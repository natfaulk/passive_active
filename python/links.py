import math

import matplotlib.pyplot as plt
import numpy as np

DEFAULT_FS=50*1000

def getPeaks(data, freqs, plot=False):
  t=np.array(data)

  # zero mean the data so there is no DC component
  t=t-np.mean(t)

  # window the data else is effectively rectangular windowed and get spectral smearing
  windowed=np.array(t)*np.hanning(len(t))
  
  # abs is to get the magnitude of the complex data returned from the fft
  freqVals=np.abs(np.fft.fft(windowed))

  # square the magnitudes to get power
  freqVals=freqVals**2

  if plot:
    plt.plot(freqVals)
    plt.show()

  out={}
  for f in freqs:
    out[f]=getSinglePeak(freqVals, f)
  return out

def freqToBin(freq, length, fs=DEFAULT_FS):
  return length*freq/DEFAULT_FS

# gets the power at a set frequency by summing adjacent bins
# vals needs to be power not magnitude!
def getSinglePeak(vals, freq):
  startBin=math.floor(freqToBin(freq, len(vals)))

  # get left side bins until starts to rise again
  i = startBin
  while vals[i] > vals[i-1] and i > 0:
    i-=1

  # and the right side
  i2 = startBin + 1
  while vals[i2] > vals[i2+1] and i2 < (len(vals) - 1):
    i2+=1

  peak=vals[i:i2+1]
  power=0
  for i in peak:
    power+=i
  return power
  