# Logging program to log from photodiode boards
When the program starts it sends the local IP of the PC to the server
The photodiode boards get the local IP of the pc from the server then directly connect to that IP over websocket. Relies on the PC and the photodiodes to be on the same subnet. Sometimes one has to reset the VLP boards to get them to connect. Once they have connected once shouldn't have trouble. The PC gets the VLP boards ID from the server and links it to the board by looking at the incoming IP of data - not ideal but the FW on the boards was designed for single board to PC only. When the program is run the first time sometimes the VLP boards have not sent their ID and IP address to the server so the board ID is blank. Restart the program and it should be fine.

The program should work fine on windows, linux or mac.

## To build
`go build`  
This will build an executable in the current directory. One can then run it.

