package main

import (
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"github.com/joho/godotenv"
	"github.com/natfaulk/nflogger"
	"github.com/natfaulk/passiveVlpRecorder/internal/comms"
	"github.com/natfaulk/passiveVlpRecorder/internal/filehandling"
)

var lg *log.Logger = nflogger.Make("Main")

var dataHeader string = "ADC Data"

func main() {
	// load env file (not actually used yet...)
	err := godotenv.Load()
	if err != nil {
		lg.Print("Error loading .env file")
	}

	// send local ip address of PC to the server
	if err := comms.SendLocalIP(); err != nil {
		lg.Println("Failed to send local IP address to the server")
		lg.Panic(err)
	}

	// get list of pd board IP addresses and their actual names as they only send raw data
	// originally the system was designed only to handle a single board, so no ID is sent
	// with the data, and the only identifying information is its local IP address which luckily
	// it sends to the server with its name and hwid each time it boots
	ipToHostname, err := comms.GetIPToHostname()
	if err != nil {
		lg.Fatal(err)
	}

	// make output data subdir
	if err := filehandling.MakeSubdirs(); err != nil {
		lg.Println("Could not make required sub directories")
		lg.Panic(err)
	}

	// Writes to the output need to be threadsafe as each websocket runs in its own go routine
	// filehandling makes a simple wrapper to achieve this
	f := filehandling.New()
	defer f.Close()

	// start the websocket server and pass in the callback to run each time data arrives
	comms.StartServer(func(msg string, ip net.Addr, timestamp time.Time) {
		if strings.HasPrefix(msg, dataHeader) {
			ipStrip := comms.StripPortFromIP(ip.String())
			output := fmt.Sprintf("%d|%s|%s|%s", timestamp.UnixNano(), ipToHostname[ipStrip], ipStrip, msg)
			lg.Printf("Message from: %s (%s), %d", ipToHostname[ipStrip], ipStrip, strings.Count(msg, dataHeader))
			// f is threadsafe as the class contains a mutex
			f.Write(output)
		}
	})
}
