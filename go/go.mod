module github.com/natfaulk/passiveVlpRecorder

go 1.14

require (
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/natfaulk/nflogger v0.0.0-20210224042024-d2129e8c7e35
)
