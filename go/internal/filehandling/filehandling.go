package filehandling

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/natfaulk/nflogger"
)

var lg *log.Logger = nflogger.Make("Filehandling")

var dataSubdir string = "data"

// OutputFile holds the file handle for the output file.
// It also has a mutex so it is threadsafe
type OutputFile struct {
	handle   *os.File
	filename string
	filepath string
	mu       sync.Mutex
}

// New creates a new output file
// if anything errors the function panics
// remember to defer f.Close()
// Also always pass the OutputFile object around by pointer, else the mutex
// gets copied by value
func New() *OutputFile {
	// generate the filename
	filename := newFilename()
	outpath, err := GetOutputFilepath(filename)
	if err != nil {
		lg.Panic(err)
	}

	out := OutputFile{}
	handle, err := os.Create(outpath)
	if err != nil {
		lg.Println("Unable to create file")
		lg.Panicln(err)
	}

	out.handle = handle
	out.filename = filename
	out.filepath = outpath

	return &out
}

// Write writes data to a file in a threadsafe manner
func (f *OutputFile) Write(data string) (int, error) {
	// important to have the mutex as multiple websockets could write simultaneously
	f.mu.Lock()
	defer f.mu.Unlock()
	return f.handle.WriteString(data)
}

// Close closes the file
func (f *OutputFile) Close() error {
	// in case still writing to the file
	f.mu.Lock()
	defer f.mu.Unlock()

	return f.handle.Close()
}

// newFilename gets a new output filename - adds current time to a prefix
func newFilename() string {
	currentTime := time.Now()
	formattedTime := currentTime.Format("2006-01-02_15-04-05")
	return fmt.Sprintf("%s_%s.txt", "PD", formattedTime)
}

// GetCurrentExecutablePath gets the path of the current executable
func GetCurrentExecutablePath() (string, error) {
	ex, err := os.Executable()
	if err != nil {
		return "", err
	}
	exPath := filepath.Dir(ex)
	return exPath, nil
}

// GetOutputFilepath is a utility method to join the output directory and output filename
func GetOutputFilepath(filename string) (string, error) {
	basePath, err := GetCurrentExecutablePath()
	if err != nil {
		lg.Println("Failed to get executable path")
		return "", err
	}

	return filepath.Join(basePath, dataSubdir, filename), nil
}

// MakeSubdirs creates the required subdirectories the program needs
func MakeSubdirs() error {
	err := os.Mkdir(dataSubdir, 0777)

	if err == nil || os.IsExist(err) {
		return nil
	}
	return err
}
