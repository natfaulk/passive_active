package comms

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

// Device represents a vlp receiver device. Is used to map the IP address to the hostname
type Device struct {
	Hostname      string `json:"hostname"`
	IP            string `json:"ip"`
	UnixTimestamp int64  `json:"timestamp"`
}

// GetDeviceList returns the latest list of devices from the server
// Is needed to map the incoming data IP addresses to the device IDs as the
// devices don't send thir ID with their data
func GetDeviceList() ([]byte, error) {
	resp, err := http.Get("http://vlp.natfaulk.com/get_ips2")
	if err != nil {
		lg.Println("Unable to get device list from server")
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		lg.Println("Unable to get device list from server")
		return nil, err
	}

	return body, nil
}

// ProcessDeviceList process the received device list byte array into a more helpful form
func ProcessDeviceList(data []byte) (map[string]string, error) {
	devices := map[string]Device{}

	if err := json.Unmarshal(data, &devices); err != nil {
		lg.Println("Failed to unmarshall device list JSON")
		return nil, err
	}

	// lg.Println(devices)

	ipToHostname := map[string]string{}

	for _, v := range devices {
		prevV, ipExists := devices[v.IP]

		if !ipExists || v.UnixTimestamp > prevV.UnixTimestamp {
			ipToHostname[v.IP] = v.Hostname
		}
	}

	return ipToHostname, nil
}

// GetIPToHostname builds a map with the device IPs as the keys and the hostname as the values
func GetIPToHostname() (map[string]string, error) {
	bytes, err := GetDeviceList()
	if err != nil {
		lg.Println("GetDeviceList from server failed.")
		return nil, err
	}

	ipHostname, err := ProcessDeviceList(bytes)
	if err != nil {
		lg.Println("ProcessDeviceList failed")
		return nil, err
	}

	return ipHostname, nil
}

// StripPortFromIP removes port from ip address
// e.g. 192.168.88.36:50538 -> 192.168.88.36
func StripPortFromIP(ip string) string {
	return strings.Split(ip, ":")[0]
}
