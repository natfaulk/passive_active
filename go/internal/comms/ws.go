package comms

import (
	"net"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

// StartServer starts the websocket server
// takes a callback as an arguement that is run each time data comes in
func StartServer(fn func(string, net.Addr, time.Time)) {
	// blindly trust any origin...
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			lg.Println("upgrade:", err)
		}

		defer ws.Close()

		for {
			_, msg, err := ws.ReadMessage()
			if err != nil {
				lg.Println("read:", err)
			} else {
				// call the callback with the incoming data as the arguments
				fn(string(msg), ws.RemoteAddr(), time.Now())
			}
		}
	})

	http.ListenAndServe(":8000", nil)
}
