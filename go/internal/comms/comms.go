package comms

import (
	"errors"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"

	"github.com/natfaulk/nflogger"
)

var lg *log.Logger = nflogger.Make("Comms")

// GetOutboundIP gets the outbound ip of this machine
// this method is better than doing lookupHost on the hostname which often returns the IP
// of the wrong interface.
func GetOutboundIP() (net.IP, error) {
	// The IP address doesn't actually have to exist or be reachable...
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP, nil
}

// SendLocalIP sends the PC's local IP to the server
func SendLocalIP() error {
	lg.Println("Getting local IP address")
	ip, err := GetOutboundIP()
	if err != nil {
		lg.Println("Couldn't get local IP")
		return err
	}
	lg.Printf("Local IP is %s", ip)

	resp, err := http.PostForm("https://vlp.natfaulk.com/set_pc_ip", url.Values{
		"ip": {ip.String()},
	})
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if string(body) != "ack" {
		lg.Println("Server did not accept our IP address")
		lg.Println("Response from server was: ", string(body))
		return errors.New("Server did not accept our IP address")
	}

	lg.Println("Successfully sent IP to server")
	return nil
}
